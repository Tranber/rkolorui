const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const directories = require('./config');

const config = {
  entry: {
    app: path.resolve(__dirname, 'src/index.tsx')
  },
  output: {
    path: path.resolve(__dirname, directories.output.devScript),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        use: ['awesome-typescript-loader'],
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },
  devServer: {
    contentBase: path.resolve(__dirname, directories.output.dev),
    historyApiFallback: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'RKolorUI',
      lang: 'fr-FR',
      base: '/',
      template: 'src/template/index.ejs',
      options: {}
    })
  ]
};

module.exports = (env, argv) => {
  // console.log('conf : ', env, argv)

  // test argv.mode : development ou production

  return config;
};
