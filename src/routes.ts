import ColorDisplay from './components/core/ColorDisplay';
import Swatch from './components/swatchs/Swatch';
import BasicColorSelector from './components/color-selectors/BasicColorSelector';

interface ComponentsList {
  [key: string]: any;
}

const components: ComponentsList = {
  'color-display': {
    component: ColorDisplay,
    title: 'Color Display',
    props: {
      r: 0.5,
      g: 0,
      b: 0.2,
      ratio: 0.5
    }
  },
  swatch: {
    component: Swatch,
    title: 'Swatch',
    props: {
      r: 0.6,
      g: 0.1,
      b: 0.2,
      ratio: 0.5,
      // systemName: 'RGB',
      title: 'Carmin',
      detail: false
    }
  },
  'basic-color-selector': {
    component: BasicColorSelector,
    title: 'Basic color selector'
  }
};

export { components };
