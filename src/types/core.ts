type Nullable<T> = T | null;
type Unknown<T> = T | undefined;
type UnknownNullable<T> = Nullable<T> | Unknown<T>;

export { Nullable, Unknown, UnknownNullable };
