import React, { useRef } from 'react';

import { useResizable, useCursor } from '../../../core/hooks';

interface HueCanvasProps {
  orientation?: 'horizontal' | 'vertical';
}

export default function HueCanvas(props: HueCanvasProps) {
  const { orientation = 'horizontal' } = props;

  const canvasRef = useRef(null);
  const cursorRef = useRef(null);
  const { width, height } = useResizable(canvasRef);
  const { coords, moveCursor, setCursor, captureCursor } = useCursor(
    cursorRef,
    width,
    height
  );

  const canvasClasses = ['hue-canvas', 'color-canvas'];
  let cursorPos: any = { left: coords.left };
  if (orientation === 'vertical') {
    canvasClasses.push('vertical-canvas');
    cursorPos = { top: coords.top };
  }

  return (
    <div
      className={canvasClasses.join(' ')}
      ref={canvasRef}
      onPointerMove={moveCursor}
      onPointerUp={setCursor}
      onPointerDown={captureCursor}
    >
      <button
        type="button"
        ref={cursorRef}
        className="color-target color-cursor"
        style={{ ...cursorPos }}
      ></button>
    </div>
  );
}
