import React, { useRef } from 'react';

import { useResizable, useCursor } from '../../../core/hooks';

interface HWBCanvasProps {
  bgColor: string;
}

export default function HWBCanvas(props: HWBCanvasProps) {
  const { bgColor } = props;

  const canvasRef = useRef(null);
  const cursorRef = useRef(null);

  const { width, height } = useResizable(canvasRef);
  const { coords, moveCursor, setCursor, captureCursor } = useCursor(
    cursorRef,
    width,
    height
  );

  return (
    <div
      ref={canvasRef}
      className="hwb-canvas color-canvas"
      style={{ backgroundColor: bgColor }}
      onPointerMove={moveCursor}
      onPointerUp={setCursor}
      onPointerDown={captureCursor}
    >
      <button
        type="button"
        ref={cursorRef}
        className="color-target color-cursor"
        style={{ top: coords.top, left: coords.left }}
      ></button>
    </div>
  );
}
