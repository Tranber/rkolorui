import React, { useRef } from 'react';

import HWBCanvas from '../core/HWBCanvas';
import HueCanvas from '../core/HueCanvas';

export default function App(props: any) {
  return (
    <div className="basic-color-selector">
      <HWBCanvas bgColor="#f00" />
      <HueCanvas />
    </div>
  );
}
