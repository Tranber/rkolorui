import React from 'react';

import ColorDisplay from '../../../components/core/ColorDisplay';

interface SwatchProps {
  r?: number;
  g?: number;
  b?: number;
  a?: number;
  systemName?: string;
  title: string;
  ratio?: number;
  detail?: boolean;
}

export default function Swatch(props: SwatchProps) {
  const {
    r = 0,
    g = 0,
    b = 0,
    a = 1,
    systemName,
    title,
    ratio = 1,
    detail = true
  } = props;
  return (
    <figure className="swatch-item">
      <ColorDisplay r={r} g={g} b={b} a={a} ratio={ratio} />
      <figcaption className="swatch-title">
        {systemName && <h3>{systemName}</h3>}
        <h4>{title}</h4>
        {detail && (
          <dl className="swatch-details">
            <dt>red</dt>
            <dd>{r * 255}</dd>
            <dt>green</dt>
            <dd>{g * 255}</dd>
            <dt>blue</dt>
            <dd>{b * 255}</dd>
          </dl>
        )}
      </figcaption>
    </figure>
  );
}
