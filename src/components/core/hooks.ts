import { useEffect, useState, useCallback, useRef } from 'react';

import { Nullable } from 'types';

function useResizable(ref: any) {
  const [width, setWidth] = useState(null);
  const [height, setHeight] = useState(null);

  useEffect(() => {
    const resizeObserver = new ResizeObserver((entries: any) => {
      for (let entry of entries) {
        if (entry.contentRect) {
          const rect = entry.contentRect;
          setWidth(rect.left + rect.width);
          setHeight(rect.top + rect.height);
        }
      }
    });

    if (ref && ref.current) {
      resizeObserver.observe(ref.current);
    }

    return () => {
      if (ref && ref.current) {
        resizeObserver.unobserve(ref.current);
      }
    };
  }, [ref]);

  return { height, width };
}

function isCursor(evt: React.SyntheticEvent): boolean {
  const elem = evt.target as HTMLElement;
  if (elem.classList.contains('color-cursor')) {
    return true;
  }
  return false;
}

function isCanvas(evt: React.SyntheticEvent): boolean {
  const elem = evt.target as HTMLElement;
  if (elem.classList.contains('color-canvas')) {
    return true;
  }
  return false;
}

function useCursor(
  cursorRef: React.MutableRefObject<Nullable<HTMLButtonElement>>,
  width: Nullable<number>,
  height: Nullable<number>
) {
  const [capture, setCapture] = useState(false);
  const [coords, setCoords] = useState({ top: 0, left: 0 });
  const deltaXCenter = useRef<number>(0);
  const deltaYCenter = useRef<number>(0);

  if (cursorRef?.current) {
    deltaXCenter.current =
      cursorRef.current.clientLeft +
      Math.ceil(cursorRef.current.clientWidth / 2);
    deltaYCenter.current =
      cursorRef.current.clientTop +
      Math.ceil(cursorRef.current.clientHeight / 2);
  }

  const captureCursor = useCallback(
    (evt: React.SyntheticEvent) => {
      if (isCursor(evt)) {
        setCapture(true);
      }
    },
    [capture]
  );

  const setCursor = useCallback(
    (evt: any) => {
      if (isCanvas(evt)) {
        if (width && height) {
          let tempLeft = evt.nativeEvent.offsetX;
          if (tempLeft < 0) {
            tempLeft = 0;
          } else if (tempLeft > width) {
            tempLeft = width;
          }
          const left = tempLeft - deltaXCenter.current;
          let tempTop = evt.nativeEvent.offsetY;
          if (tempTop < 0) {
            tempTop = 0;
          } else if (tempTop > height) {
            tempTop = height;
          }
          const top = tempTop - deltaYCenter.current;

          setCursorCoords(left, top);
        }
      }
      if (isCursor(evt)) {
        setCapture(false);
      }
    },
    [width, height]
  );

  const setCursorCoords = (left: number, top: number) => {
    setCoords({ left, top });
  };

  const moveCursor = useCallback(
    (evt: any) => {
      if (capture === true && isCanvas(evt)) {
        if (width && height) {
          let tempLeft = evt.nativeEvent.offsetX;
          if (tempLeft < 0) {
            tempLeft = 0;
          } else if (tempLeft > width) {
            tempLeft = width;
          }
          const left = tempLeft - deltaXCenter.current;

          let tempTop = evt.nativeEvent.offsetY;
          if (tempTop < 0) {
            tempTop = 0;
          } else if (tempLeft > height) {
            tempTop = height;
          }
          const top = tempTop - deltaYCenter.current;

          setCursorCoords(left, top);
        }
      }
    },
    [capture, width, height, deltaXCenter.current, deltaYCenter.current]
  );

  return { coords, moveCursor, setCursor, captureCursor };
}

export { useResizable, useCursor };
