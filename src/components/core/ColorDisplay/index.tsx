import React, { useRef } from 'react';
import { RGBColor, CSSColor } from 'koloriloj';

interface ColorDisplayProps {
  r?: number;
  g?: number;
  b?: number;
  a?: number;
  ratio?: number;
}

export default function ColorDisplay(props: ColorDisplayProps) {
  const { r = 0, g = 0, b = 0, a = 1, ratio = 1 } = props;
  const bgColor = CSSColor.toString(new RGBColor(r, g, b, a));
  const styles = {
    paddingTop: `${ratio * 100}%`,
    backgroundColor: bgColor
  };
  return <div className="color-display" style={styles}></div>;
}
