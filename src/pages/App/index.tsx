import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import BasicColorSelector from '../../components/color-selectors/BasicColorSelector';
import Swatch from '../../components/swatchs/Swatch';

import Home from '../Home';
import Component from '../Component';

export default function App(props: any) {
  return (
    <Router>
      <Route path="/" exact={true} component={Home} />
      <Route path="/component/:componentId" component={Component} />
    </Router>
  );
}
