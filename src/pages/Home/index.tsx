import React from 'react';
import { Link } from 'react-router-dom';

import { components } from '../../routes';

export default function Home(props: any) {
  return (
    <>
      <header>
        <h1>Accueil</h1>
      </header>
      <ul className="components-list">
        {Object.keys(components).map((key, index) => {
          const component = components[key];
          if (component) {
            const { title, props } = component;
            return (
              <li className="component-item" key={`component-item-${index}`}>
                <Link
                  to={{
                    pathname: `/component/${key}`,
                    state: { key, title, props }
                  }}
                >
                  {title}
                </Link>
              </li>
            );
          }
          return null;
        })}
      </ul>
    </>
  );
}
