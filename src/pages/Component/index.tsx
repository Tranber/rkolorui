import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import { components } from '../../routes';

interface RouteState {
  key: string;
  title: string;
  props?: any;
}

export default function Component(props: any) {
  const { state } = useLocation<RouteState>();
  const { key, title, props: componentProps } = state;
  const ComponentObj = components[key] && components[key].component;

  return (
    <>
      <header>
        <h1>{title}</h1>
        <Link to="/">Accueil</Link>
      </header>
      <main>{ComponentObj && <ComponentObj {...componentProps} />}</main>
    </>
  );
}
