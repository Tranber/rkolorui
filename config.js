const src = './src/';
const dest = './dist/';
const dev = './build/';

const directories = {
  input: {
    src,
    html: `${src}assets/html/`,
    style: `${src}assets/scss/`
  },
  output: {
    dest,
    style: `${dest}css/`,
    dev,
    devStyle: `${dev}css/`,
    devScript: `${dev}js/`
  }
};

module.exports = directories;
