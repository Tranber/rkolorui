const gulp = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('sass');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const rename = require('gulp-rename');

const directories = require('./config');

function styles() {
  return gulp
    .src([
      `${directories.input.style}components.scss`,
      `${directories.input.src}/components/**/*.scss`
    ])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(concat('rkolorui.css'))
    .pipe(gulp.dest(directories.output.style))
    .pipe(gulp.dest(directories.output.devStyle))
    .pipe(cssnano())
    .pipe(rename('rkolorui.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(directories.output.style));
}

gulp.task('styles', styles);

function demoStyles() {
  return (
    gulp
      .src([`${directories.input.style}core.scss`])
      // .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(concat('core.css'))
      // .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(directories.output.devStyle))
    // .pipe(cssnano())
    // .pipe(rename('rkolorui.min.css'))
    // .pipe(gulp.dest(directories.output.style))
  );
}

gulp.task('demoStyles', demoStyles);

gulp.task('default', gulp.parallel('styles', 'demoStyles'));

gulp.task(
  'assets',
  gulp.parallel('styles', 'demoStyles', () => {
    gulp.watch(`${directories.input.src}/components/**/*.scss`, styles);
    gulp.watch(`${directories.input.style}**/*.scss`, demoStyles);
  })
);
